#!/usr/bin/env python

from distutils.core import setup

setup(name='dater',
      version='0.5',
      description='Very simple python script to parse time-zoned appointments and import them in Apple calendar.',
      author='Yan Grange',
      author_email='grange@astron.nl',
      py_modules = ['dater'],
      install_requires=[
        'ics>=0.7',
        'dateparser>=1.1.0'
    ],
    license="Apache License, Version 2.0",
    entry_points={
        'console_scripts': ['dater=dater:main']
    }
)
