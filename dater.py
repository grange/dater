#!/usr/bin/env python

import dateparser
import subprocess as SP
from ics import Calendar, Event
from datetime import timedelta
from tempfile import mktemp
from sys import argv
import pytz

def main():
    date_in = False
    while not date_in:
        date_time = input("Date as worded in the email: ")
        parsed_date = dateparser.parse(date_time)
        if not parsed_date:
            print("Could not parse the date.")
            continue
        print("Date parsed as: ")
        print(parsed_date.strftime("Date: %Y-%m-%d (month %b), time: %H:%M:%S, time zone: %Z"))
        date_in = input("agree? ").lower() in ["y", "yes", 1, "true", "ok", "sure", "fine"]
    description = input("Description of the meeting: ")
    duration = float(input("Duration of the meeting (in hours): "))
    default_timezone = pytz.timezone('Europe/Amsterdam')
    try:
        parsed_date = default_timezone.localize(parsed_date)
    except ValueError: # tz already set
        pass
    evt = Event()
    evt.begin = parsed_date.isoformat()
    evt.duration = timedelta(hours=duration)
    evt.name = description
    cal = Calendar()
    cal.events.add(evt)

    print("This is the data:")
    icsfile = mktemp(suffix=".ics")

    with open(icsfile, "w") as write_file:
        write_file.write(cal.serialize())
    
    SP.call(["open", "-a", "Microsoft Outlook", icsfile])

if __name__ == '__main__':
    main()

