# dater

Very simple python script to parse time-zoned appointments and import them in Outlook (on Mac).

## Usage
You can simply run `dater.py` and it will ask for the date string, description, and duration of the appointment. This will all be taken and if the data is parsable by dateparser, it will generate an `.ics` file in `/tmp` and open it which will make iCalendar pop up.

## Known issues
If the date is in the format `11 jan at 20 UTC`, this may actually be interpreted as `11 january 2020`. So please check if the time string makes sense to you. 
